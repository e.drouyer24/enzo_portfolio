<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="style/style.css">
<link rel="stylesheet" href="./style/media.css">
  <style>   @import url("https://use.typekit.net/mrl4lgo.css");</style>
<head>
  <body>
    <?php require 'header.php' ?>
    <section class="contact-page">
      <div> <img  class="svg-contact-header"src="img\forme_bleu_contact_header.svg"></div>

      <div class="img-contact-container"> <img class="img-contact"src="img\illustration 6 contact-03.png" alt="Image d'illustration representant un personnage allant chercher des courriers"> </div>
      <div class="forms">
      <form action="" method="post">
        <div class="form-name">
          <label for="name">Nom : </label>
          <input type="text" name="first_name">
        </div>
        <div class="form-mail">
          <label for="email">Adresse mail : </label>
          <input type="text" name="email">
        </div>
        <div class="form-message">
          <label for="message">Votre message : </label>
          <textarea rows="5" name="message" cols="30" rows="25"></textarea>
        </div>
        <input class="bouton-envoi" type="submit" name="submit" value="Envoyer">
      </form>
          <?php
          if(isset($_POST['submit'])){
              $to = "e.drouyer24@outlook.fr";             // this is your Email address
              $from = $_POST['email'];                // this is the sender's Email address
              $first_name = $_POST['first_name'];
              $subject = "Form submission";
              $subject2 = "Copy of your form submission";
              $message = $first_name . " " . " wrote the following:" . "\n\n" . $_POST['message'];
              $message2 = "Here is a copy of your message " . $first_name . "\n\n" . $_POST['message'];

              $headers = "From:" . $from;
              $headers2 = "From:" . $to;
              mail($to,$subject,$message,$headers);
              mail($from,$subject2,$message2,$headers2);                                        // sends a copy of the message to the sender
//    echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
              // You can also use header('Location: thank_you.php'); to redirect to another page.
          }
          $nom=$_POST["nom"];
$email=$_POST["email"];
$form_message=$_POST['message'];
$valider=$_POST["valider"];

if(isset($valider)){
    if(empty($nom))
        $message='<div class="erreur">Nom laissé vide.</div>';
    elseif(empty($form_message))
        $message='<div class="erreur">Message vide laissé vide.</div>';
    elseif(empty($email))
        $message='<div class="erreur">Email laissé vide.</div>';

    else{
        $message='<div class="rappel"><b>Rappel:</b><br />';
       strtoupper($nom).'<br />';
        $message.='Email: '.$email;
        $message.='</div>';
    }
}?>
      </div>


        <div> <img  class="svg-contact-footer"src="img\forme_bleu_contact_footer.svg"></div>
    </section>
    <?php  require 'news.php'?>
<script src="script.js"></script>
  </body>
