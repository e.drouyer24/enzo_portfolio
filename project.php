<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="style/style.css">
  <link rel="stylesheet" href="style/media.css">
    <style>   @import url("https://use.typekit.net/mrl4lgo.css");</style>
</head>
 <!--J'AI MIT UN MENU 2 sur le header_style2 a modifier avec le css lors du responsive (MENU BURGERRRRRRRR) -->
<body>
  <?php require 'header_style2.php' ?>
  <section class="project-page">
      <div> <img  class="svg-project"src="img\forme_violet_header.svg"></div>
    <div> <img class="img-project"src="img\illustration 5-03.png" alt="Image d'illustration representant un personnage recherchant un projet"> </div>
    <div class="project-info">
      <h4>Mes projets<h4>
        <p>Bienvenue sur la page de mes différents projets, en cours de formation, projet de stage ou
        encore des projets perso. C'est une "base de donnée" qui répertorie toutes mes réalisations</p>
            <button class="project-button" type="button"> Ensuite </button>
    </div>
  </section>
  <?php  require 'news.php'?>
  <section class="project-container">
    <div class="project-1">
    <img class="project-newsletter" src="./img/nesletter.PNG">
    <h5 class="newsletter-title">Newsletter 2021</h5>
    </div>
    <div class="project-2">
      <img class="project-association" src="./img/les_2_rives.PNG" alt="">
      <h5>Les 2 rives association</h5>
    </div>
    <div class="project-3">
        <img class="project-homerun" src="./img/website_homerun.png" alt="">
        <h5>Website - Home RUN</h5>
    </div>
  </section>

  <section class="project-container">
    <div class="project-1">

    </div>
    <div class="project-2">

    </div>
    <div class="project-3">

    </div>
  </section>

  <section class="project-container">
    <div class="project-1">

    </div>
    <div class="project-2">

    </div>
    <div class="project-3">

    </div>
  </section>
</body>
<footer class="project-foot">

  <p class="copyright-project">© 2021, Enzo Drouyer</p>
  <p class="return-top-project">Back to top</p>
  <div> <img  class="svg-violet"src="img\forme_violet_footer.svg"></div>
</footer>
