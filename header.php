<header>
  <nav role="navigation">
    <p class = "title">Enzo Drouyer</p>
      <div id="menuToggle">
        <!--
       Une fausse case a cocher faisant office de receveur,
        -->
        <input class="input1"type="checkbox" />
        <!--
        Des spans pour composer le burger
        -->
        <span></span>
        <span></span>
        <span></span>

        <!--
        Le menu doit etre contenu dans un bouton
        -->
        <ul id="menu">
          <a href="./index.php"><li>Accueil</li></a>
          <a href="./project.php"><li>Les projets</li></a>
          <a href="./contact.php"><li>Contact</li></a>
        </ul>
      </div>
    </nav>
</header>
