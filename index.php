<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,
     initial-scale=1, shrink-to-fit=no">
    <title>Enzo Drouyer</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="./style/media.css">
    <style>   @import url("https://use.typekit.net/mrl4lgo.css");</style>
  </head>
  <body>
      <?php require 'header.php' ?>

      <section class="big-container-hello">
        <div> <img  class="svg-header-home"src="img\forme_bleu_ header_home.svg"></div>
        <div class="hello">
          <h1>Etudiant, developpeur web</h1>
          <h2>Depuis plus de deux ans j'etudie dans le domaine du développement web, en tant que junior j'ai appréhender
          différents languages mêlant front end et back end.</h2>
             <button class="button-home" type="button"> Ensuite </button>
        </div>
        <div class="hello-container">
        <img class="accueil-hello"src="img/image 1 Hello-03.png" alt="Image d'illustration representant un personnage le pied sur un ecran">
        </div>
      </section>
      <?php  require 'news.php'?>
      <section>
        <h3 class="solution-home">Solutions en place</h3>
        <div> <img  class="svg-jaune-home"src="img\forme_jaune_home.svg"></div>
        <article class="draw-home">
        <div> <img class="img-draw"src="img\image_2_dessin_03.png" alt="Image d'illustration representant un personnage peignant sur une toile vierge"> </div>
        <div class="drawing-info">
          <h4 class="drawing-title">Réalisation de design </h4>
          <p class="drawing-text">Du papier à l'écran je concrétise toutes vos idées afin de les mettre en forme.
              Grâce à mes outils de pao, je suis libre d'effectuer tout types de sites, logos, menus, chevalet et tout autres support de communication selon vos besoins</p>
        </div>
        </article>
        <div> <img  class="svg-orange-home"src="img\forme_orange_accueil.svg"></div>
        <article class="code-home">
          <div class="code-info">
            <h4>Réalisation du code<h4>
              <p>De l'éditeur au serveur, je développe toutes vos demande de sites que ce soit du HTML/CSS language primaire
              pour l'affichage d'une page web, ajout de dynamisme coté front (Javascript), ajout de dynamisme coté serveur (PHP)</p>
          </div>
          <div> <img class="img-code"src="img\illustration 3 code-03.png" alt="Image d'illustration representant un personnage codant sans relâche"> </div>
        </article>

        <article class="services-home">
          <div> <img class="img-services"src="img\illustration 4 solutions-03.png" alt="Image d'illustration representant deux personnages concluant un accord sur la technologie a utilisé"> </div>
          <div class="services-info">
             <h4>Accompagnement personnalisé<h4>
              <p>Quelque soit votre projet je vous accompagne vers une solution la plus efficace et cohérente en fonction de vos besoins
              Proposition d'utilisation de CMS ou de framework en fonction de l'utilité.</p>
          </div>
        </article>
          <div> <img  class="svg-bleu-home"src="img\forme_bleu_footer.svg"></div>
        <p class="now-home">Actuellement étudiant en developpement web/web mobile. J'aspire a devenir Web-Designer
        Je suis aussi passionné de calligraphie et de dessin.</p>
      </section>

      <footer>

        <p class="copyright-home">© 2021, Enzo Drouyer</p>
        <p class="return-top-home">Back to top</p>
      

      <footer>
  </body>
</html>
