//Fonction click pour fermer la fenetre
document.getElementById('close').addEventListener("click",function(){
    document.getElementById('newsletter-contain').style.display = "none";
    document.getElementById('close-news').style.display = "none";
    document.getElementById('close-news2').style.display = "none";
})
const sendElement = document.querySelector('input[type="submit"]')
const messageContainer = document.getElementById("message-container")

function notEmpty(value){
    return value.length > 0
}

function minLength(value){
    return value.length >= 8
}

const rules = [
    {
        id: "username",
        validator: minLength,
        message: "Identifiant trop court"
    },
    {
        id: "username",
        validator: notEmpty,
        message: "Identifiant obligatoire"
    },

    {
        id: "mail",
        validator: function (value){
            const atPos = value.indexOf('@')
            const dotPos = value.lastIndexOf('.')
            if(atPos === -1 || dotPos === -1){
                return false
            }
            return atPos < dotPos
        },
        message: "Le mail est invalide"
    },

]
let error = "Certains champs sont incorrect";
let succes = "Aucune erreur les informations sont valides"
sendElement.addEventListener("click", function (){
    error = document.createElement("div")
    error.classList.add("error")
    success = document.createElement("div")
    success.classList.add("success")
    success.innerHTML = "compte créé"
    var hasErrors = false
    for (rule of rules){
        const inputValue = document.getElementById(rule.id).value
        if(!rule.validator(inputValue)){
            hasErrors = true;
            error.innerHTML+=rule.message+"\n"
        }
    }
    messageContainer.innerHTML = ""
    messageContainer.appendChild(hasErrors?error:success)
})
